from flask import Flask, render_template, abort
from lxml import etree
app=Flask(__name__)

@app.route('/')
def Inicio():
    nombre='Omar Elhani Botkala'
    return render_template('Inicio.html', nombre=nombre)

@app.route('/PotenciasDinamico/<base>/<exponente>')
def PotenciasDinamico(base, exponente):
    if int(exponente) > 0:
        resultado = int(base)**int(exponente)
    elif int(exponente) ==0:
        resultado = 1
    elif int(exponente) < 0:
        resultado = 1/(int(base)**int(exponente))
    else:
        return abort(404)
    return render_template('PotenciasDinamico.html', resultado=resultado, base=base, exponente=exponente)


@app.route('/Contar/<palabra>/<letra>')
def cuenta(palabra, letra):
    if len(letra) == 1:
        cantidad = palabra.count(letra)
        return render_template('Contar.html', cantidad=cantidad, palabra=palabra, letra=letra)
    else:
        return abort(404)

@app.route('/libro/<codigo>')
def libros(codigo):
    doc = etree.parse('libros.xml')
    if str(codigo) in doc.xpath("/biblioteca/libro/codigo/text()"):
        titulo= doc.xpath('/biblioteca/libro[codigo="%s"]/titulo/text()' %(codigo))[0]
        nom_autor= doc.xpath('/biblioteca/libro[codigo="%s"]/autor/text()' %codigo)[0]
        return render_template('libros.html', nom_libro=titulo, nom_autor=nom_autor)
    else:
        return abort(404)

app.run(debug=True)